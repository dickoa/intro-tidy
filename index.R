## ----message=FALSE, warning=FALSE,echo=FALSE-----------------------------
# library(kable)
knitr::opts_chunk$set(warning = FALSE, message = FALSE, fig.width=12, fig.height=4)

## ---- echo = FALSE-------------------------------------------------------
library(DSR)
library(DT)
library(tidyverse) # Importer tidyverse
datatable(table1, options = list(dom = 't'))

## ---- echo = FALSE-------------------------------------------------------
datatable(table2, options = list(dom = 't'))

## ---- echo = FALSE-------------------------------------------------------
datatable(table3, options = list(dom = 't'))

## ----eval = FALSE--------------------------------------------------------
## install.packages("tidyverse") ## télécharger le package

## ----Import tidyverse----------------------------------------------------
library(tidyverse) # Importer tidyverse

## ------------------------------------------------------------------------
library(readxl)

## ----Read in excel data--------------------------------------------------
raw_tb <- read_excel(path = "data/tabular/tidy_data.xlsx", sheet = 2)

## ---- echo = FALSE-------------------------------------------------------
raw_tb

## ------------------------------------------------------------------------
library(tidyr)

## ------------------------------------------------------------------------
spread(raw_tb, key, value)

## ------------------------------------------------------------------------
raw_tb <- read_excel(path = "data/tabular/tidy_data.xlsx", sheet = 4)

## ---- echo = FALSE-------------------------------------------------------
raw_tb

## ------------------------------------------------------------------------
gather(raw_tb, key = "year", value = "cases", 2:3) ## no

## ------------------------------------------------------------------------
library(readr)

## ------------------------------------------------------------------------
raw_weather <- read_csv(file = "data/tabular/weather_tmax.csv", na = "NA")

## ---- echo = FALSE-------------------------------------------------------
raw_weather[, 1:10]

## ------------------------------------------------------------------------
gather(raw_weather, key = "day", value = "tmax", d1:d31, na.rm = TRUE)

## ---- echo = FALSE-------------------------------------------------------
clean_weather <- gather(raw_weather, key = "day", value = "tmax", d1:d31, na.rm = TRUE)
clean_weather$tmax <- parse_number(clean_weather$tmax)
clean_weather$day <- parse_number(clean_weather$day)

## ------------------------------------------------------------------------
clean_weather %>%
  group_by(month) %>%
  summarise(tmax_moy = mean(tmax), tmax = max(tmax))

## ------------------------------------------------------------------------
clean_weather %>%
  unite(date, year, month, day, sep = "-") %>%
  mutate(date = as.Date(date)) %>%
  ggplot(aes(date, tmax)) +
    geom_line()

## ------------------------------------------------------------------------
library(DBI)
library(dbplyr)
conn <- dbConnect(RSQLite::SQLite(),  "data/db/taille.db")

## ------------------------------------------------------------------------
dbListTables(conn)
dbListFields(conn, "taille")

## ------------------------------------------------------------------------
dbGetQuery(conn, "SELECT * FROM taille")

## ------------------------------------------------------------------------
dbGetQuery(conn, "SELECT nom from taille WHERE taille > 165")

## ------------------------------------------------------------------------
df <- tbl(conn, "taille")
df

## ------------------------------------------------------------------------
df %>%
  filter(taille > 165) %>%
  select(nom)

## ------------------------------------------------------------------------
df %>%
  filter(taille > 165) %>%
  select(nom) %>%
  collect()

## ---- echo = FALSE-------------------------------------------------------
dbDisconnect(conn)

## ------------------------------------------------------------------------
library(sf)
village <- read_sf("data/vector/loc/loc.shp")

## ------------------------------------------------------------------------
village

## ------------------------------------------------------------------------
routes <- read_sf("data/vector/routes/roads.shp")

## ------------------------------------------------------------------------
glimpse(routes)

## ------------------------------------------------------------------------
region <- read_sf("data/vector/reg/reg.shp")

## ------------------------------------------------------------------------
glimpse(region)

## ---- eval = FALSE-------------------------------------------------------
## ggplot() +
##   geom_sf(data = region) +
##   geom_sf(data = village, size = 0.1, shape = 21) +
##   geom_sf(data = routes, colour = "steelblue", size = 1.5)

## ---- echo = FALSE, fig.width=12, fig.height=6---------------------------
ggplot() +
  geom_sf(data = region) +
  geom_sf(data = village, size = 0.1, shape = 21) +
  geom_sf(data = routes, colour = "steelblue", size = 1.5) 

